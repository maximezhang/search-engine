import sys

from search.document import Document, TokenizedDocument
from search.index import Index
from search.posting import Posting
from utils.text_processor import Normalizer

WORD_PROCESSORS = [
    Normalizer(),
]


def get_index(path: str) -> Index:
    """
    Return index of a dataset
    """
    print(f'Indexing... {path}')

    documents = Document.fetch(path)
    print(f'  * Fetched {len(documents)} documents')

    tokenized_documents = TokenizedDocument.analyze(documents, WORD_PROCESSORS)
    print(f'  * Tokenized {len(tokenized_documents)} documents')

    postings = Posting.index(tokenized_documents)
    print(f'  * Created {len(postings)} postings')

    index = Index.build(postings)
    print(f'  * Built index with {len(index.url_to_did)} urls and {len(index.word_to_dids)} words')

    return index


def run_search(index: Index):
    """
    Run the search engine
    """
    while True:
        word = input('mono word search (enter to skip)> ')
        if len(word) == 0:
            break

        print(f'* Searching word \'{word}\'')

        matches = index.search(word, WORD_PROCESSORS)
        for url in matches:
            print(f'** Found \'{word}\' in {url}')
        print(f'{len(matches)} results')

    while True:
        words = input('all word search (enter to skip)> ')
        if len(words) == 0:
            break

        print(f'* Searching all words \'{words}\'')

        matches = index.search_all(words.split(), WORD_PROCESSORS)
        for url in matches:
            print(f'** Found \'{words}\' in {url}')
        print(f'{len(matches)} results')

    while True:
        words = input('any word search (enter to skip)> ')
        if len(words) == 0:
            break

        print(f'* Searching one word from \'{words}\'')

        matches = index.search_one(words.split(), WORD_PROCESSORS)
        for url in matches:
            print(f'** Found \'{words}\' in {url}')
        print(f'{len(matches)} results')


def main():
    if len(sys.argv) == 1:
        print(f'usage: python {__file__} path/to/dataset')
        return

    get_index(sys.argv[1]).save('index.bin')
    index2 = Index.load('index.bin')

    run_search(index2)
    print('Exiting...')


if __name__ == '__main__':
    main()
