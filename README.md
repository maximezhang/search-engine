# Search engine
Simple search engine written en python3.

## Usage
```shell script
$ python search_engine path/to/dataset
```

## Authors
- maxime.zhang, <maxime.zhang@epita.fr>
