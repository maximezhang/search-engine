from typing import List

from search.document import TokenizedDocument


class Posting:
    def __init__(self, word: str, urls: List[str]) -> None:
        self.word = word
        self.urls = urls

    @staticmethod
    def index(documents: List[TokenizedDocument]) -> List['Posting']:
        posting = {}
        for document in documents:
            for word in document.words:
                if word not in posting:
                    posting[word] = []
                posting[word].append(document.url)

        return [Posting(word, urls) for word, urls in posting.items()]
