import pickle

from typing import List

from search.posting import Posting
from utils.text_processor import TextProcessor


class Index:
    def __init__(self) -> None:
        self.url_to_did = {}
        self.did_to_url = {}
        self.word_to_dids = {}

    @staticmethod
    def build(postings: List[Posting]) -> 'Index':
        index = Index()
        for posting in postings:
            if posting.word not in index.word_to_dids:
                index.word_to_dids[posting.word] = []
            for url in posting.urls:
                if url not in index.url_to_did:
                    did = len(index.url_to_did)
                    index.url_to_did[url] = did
                    index.did_to_url[did] = url

                index.word_to_dids[posting.word].append(index.url_to_did[url])

        return index

    def save(self, path: str) -> None:
        with open(path, 'wb') as f:
            f.write(pickle.dumps(self))

        print(f'Saved index to {path}')

    @staticmethod
    def load(path: str) -> 'Index':
        with open(path, 'rb') as f:
            index = pickle.loads(f.read())

        print(f'Loaded index from {path}')
        return index

    def search(self, word: str, query_word_processors: List[TextProcessor]) -> List[str]:
        """
        Search urls that contain word
        """
        for processor in query_word_processors:
            word = processor.process(word)

        if word not in self.word_to_dids:
            return []

        return sorted({self.did_to_url[did] for did in self.word_to_dids[word]})

    def search_all(self, words: List[str], processors: List[TextProcessor]) -> List[str]:
        """
        Search urls that contain every word from words
        """
        matches = set(self.search(words[0], processors))
        for word in words[1:]:
            matches.intersection_update(self.search(word, processors))
        return sorted(matches)

    def search_one(self, words: List[str], processors: List[TextProcessor]) -> List[str]:
        """
        Search urls that contain any word from words
        """
        matches = set()
        for word in words:
            matches.update(self.search(word, processors))
        return sorted(matches)
