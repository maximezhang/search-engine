import os
import re

from typing import List

from utils.text_processor import TextProcessor


class Document:
    def __init__(self, url: str) -> None:
        with open(url, 'r') as f:
            self.text = f.read()
        self.url = url

    @staticmethod
    def fetch(path) -> List['Document']:
        return [Document(os.path.join(path, url)) for url in os.listdir(path)]


class TokenizedDocument:
    def __init__(self, document: Document, word_processors: List['TextProcessor']) -> None:
        self.words = [
            processor.process(word)
            for word in re.compile(r"[!\"#$%&()*+,'\-./:;<=>?@[\\\]^_`{|}~\s]+").split(document.text)
            for processor in word_processors
            if len(word) > 0
        ]
        self.url = document.url

    @staticmethod
    def analyze(documents: List[Document], word_processors: List['TextProcessor']) -> List['TokenizedDocument']:
        return [TokenizedDocument(document, word_processors) for document in documents]
