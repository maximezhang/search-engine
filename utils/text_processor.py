import unicodedata

from abc import ABC, abstractmethod


class TextProcessor(ABC):
    @abstractmethod
    def process(self, word: str) -> str:
        pass


class Normalizer(TextProcessor):
    def process(self, word: str) -> str:
        return unicodedata.normalize('NFKD', word).encode('ascii', 'ignore').decode('utf-8').lower()
